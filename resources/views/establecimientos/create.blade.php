@extends('layouts.app')

@section('styles')

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet@1.6.0/dist/leaflet.css" />
<!-- Esri Leaflet Geocoder -->
<link rel="stylesheet" type="text/css" href="http://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">
<!-- Geosearch Leaflet -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-geosearch@3.0.0/dist/geosearch.css"/>

@endsection

@section('content')
    <div class="container">
        <h1 class="text-center mt-4">Registrar establecimiento</h1>
        <div class="mt-5 row justify-content-center">
            <form class="col-md-9 col-xs-12 card card-body" action="#" method="post">
                <fieldset class="border p-4">
                    <legend class="text-secondary">Nombre, categoría e imagen principal</legend>
                    <div class="form-group">
                        <label for="name">
                            Nombre establecimiento
                        </label>
                        <input id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nombre establecimiento" value="{{old('name', $establecimiento->name)}}">

                        @error('name')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="category">
                            Categoría
                        </label>
                        <select name="categoria_id" id="category" class="form-control @error('categoria_id') is-invalid @enderror">
                            <option value="" disabled selected>-- Categoría --</option>
                            @foreach ($categorias as $categoria)
                                <option value="{{$categoria->id}}" {{old('categoria_id', $establecimiento->categoria_id) == $categoria->id ? 'selected' : ''}}>{{$categoria->name}}</option>
                            @endforeach
                        </select>

                        @error('categoria_id')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group custom-file mt-4">
                        <label for="image" class="custom-file-label">Seleccionar imagen</label>
                        <input type="file" class="custom-file-input @error('image') is-invalid @enderror" name="image">

                        @error('image')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                        @enderror
                    </div>

                </fieldset>

                <fieldset class="border p-4 mt-3">
                    <legend class="text-secondary">Ubicación</legend>
                    <div class="form-group">
                        <label for="formbuscador">
                            Coloca la dirección del establecimiento
                        </label>
                        <input id="formbuscador" type="text" class="form-control" placeholder="Calle del establecimiento">
                        <p class="text-secondary mt-4 mb-3 text-center">El asistente colocará una dirección estimada, mueve el pin hacía el lugar correcto.</p>
                    </div>

                    <div class="form-group">
                        <div id="mapa" style="height: 350px;"></div>
                    </div>

                    <p class="informacion">Confirma que los siguientes datos son correctos.</p>

                    <div class="form-group">
                        <label for="address">Dirección</label>

                        <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" placeholder="Dirección" value="{{old('address', $establecimiento->address)}}">

                        @error('address')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        <label for="suburb">Colonia</label>

                        <input type="text" class="form-control @error('suburb') is-invalid @enderror" id="suburb" placeholder="Colonia" value="{{old('suburb', $establecimiento->suburb)}}">

                        @error('suburb')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                        @enderror
                    </div>

                    <input type="hidden" name="lat" id="lat" value="{{old('lat', $establecimiento->lat)}}">

                    <input type="hidden" name="lng" id="lng" value="{{old('lng', $establecimiento->lng)}}">
                </fieldset>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/leaflet@1.6.0/dist/leaflet.js">
</script>
<script src="https://unpkg.com/esri-leaflet"></script>
<script src="https://unpkg.com/esri-leaflet-geocoder"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet-geosearch@3.0.0/dist/geosearch.umd.js"></script>
@endsection