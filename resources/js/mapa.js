document.addEventListener('DOMContentLoaded', () => {

    if (document.querySelector('#mapa')) {
        const lat = 18.251128;
        const lng = -93.223562;
    
        const mapa = L.map('mapa').setView([lat, lng], 16);
    
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mapa);
    
        let marker;
    
        // agregar el pin
        marker = new L.marker([lat, lng], {
            draggable:true,
            autoPan: true,

        }).addTo(mapa);      

        const geocodeService = L.esri.Geocoding.geocodeService();

        marker.on('moveend', function (e) {
            marker = e.target;

            const position = marker.getLatLng();


            mapa.panTo(new L.LatLng(position.lat, position.lng))

            //Revers Geocoding
            geocodeService.reverse().latlng(position, 16).run(function (error, resultado) {
                console.log(error)
                console.log(resultado)
            })
        })
    }
})